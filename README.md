# RabbitMQ

Owner: Julian De La Rosa

# InkBytes RabbitMQ Docker Setup

**Owner**: Julian De La Rosa

This repository provides the necessary configuration and files to run a RabbitMQ server within a Docker container. It's designed to streamline the process of setting up, configuring, and managing a RabbitMQ server environment using Docker.

## Project Structure

- **`Dockerfile`**: Contains instructions for Docker to build the RabbitMQ image.
- **`docker-compose.yml`**: Defines and runs the RabbitMQ service in Docker.
- **`rabbitmq.conf`**: The main configuration file for the RabbitMQ server.
- **`data/`**: Holds RabbitMQ server data, including the Erlang cookie, Mnesia database, and schema.
- **`logs/`**: Stores server logs and crash dumps for analysis and troubleshooting.
- **`plugins/`**: Contains RabbitMQ server plugins and their respective configuration files.

## Setup Instructions

Before you start, ensure you have Docker and Docker Compose installed on your system. Follow these steps to get your RabbitMQ server up and running:

1. **Start the Server**:
Run the following command to start the RabbitMQ server in a Docker container:
    
    ```
    docker-compose up
    
    ```
    
2. **Access the Management Interface**:
After the server starts, you can access the RabbitMQ management interface at [http://localhost:15672](http://localhost:15672/).

## Docker File Configuration

The Dockerfile sets up the RabbitMQ environment:

```
FROM rabbitmq:3.7-management

COPY ./plugins/rabbitmq.conf /etc/rabbitmq
COPY ./plugins/definitions.json /etc/rabbitmq
COPY ./plugins/advanced.config /etc/rabbitmq

RUN cat /etc/rabbitmq/rabbitmq.conf
EXPOSE 15671 15672 61613 61614 1883 8883 15675 5672 15670 15674

```

This Dockerfile uses the official RabbitMQ image with management plugin, copies necessary configuration files, and exposes various ports for different services and protocols.

## Docker Compose Configuration

The `docker-compose.yml` file is used to define and manage the multi-container Docker application:

```yaml
version: "3.2"
services:
  rabbitmq:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: "rabbitmq"
    ports:
      - 5672:5672
      - 15672:15672
      - 15674:15674
      - 15670:15670
      - 15673:15673
      - 61613:61613
    volumes:
      - ./data/:/var/lib/rabbitmq
      - ./logs/:/var/log/rabbitmq
      - ./plugins:/etc/rabbitmq
    restart: unless-stopped

```

This configuration builds the RabbitMQ service, sets container names, maps necessary ports, mounts volumes for persistent data, logs, and plugins, and ensures the service restarts unless manually stopped.

## Enabled Plugins

For detailed information about RabbitMQ plugins and how to manage the enabled plugins file, refer to the official documentation: [Plugins](https://www.rabbitmq.com/plugins.html#enabled-plugins-file).

## Stomp & Stomp-Web Configuration

This section configures the STOMP and Web STOMP plugins with specified user credentials and TCP options, sets up the management plugin to listen on a designated port, ensures RabbitMQ listens for standard AMQP connections, and requires explicit connections for Web STOMP:

```erlang
[{rabbitmq_stomp,[{default_user,[{passcode,"guest"},{login,"guest"}]},
                  {tcp_listen_options,[{sndbuf,131072},
                                       {send_timeout,120},
                                       {recbuf,131072},
                                       {keepalive,true},
                                       {exit_on_close,true},
                                       {nodelay,true},
                                       {backlog,4096}]},
                  {default_vhost,<<"/">>}]},
 {rabbitmq_management,[{tcp_config,[{port,15672}]}]},
 {rabbit,[{tcp_listeners,[5672]},{loopback_users,[]}]},
 {rabbitmq_web_stomp,[{implicit_connect,false}]}].

```

## License

This project is licensed under the MIT License - see the [LICENSE.md](http://license.md/) file for details.

---

Feel free to contribute or suggest improvements to this project. If you encounter any issues, please open a ticket in the repository's issue tracker.

# **RabbitMQ Docker Setup**

This repository contains the necessary configuration and setup files for running a RabbitMQ server in a Docker container.

## **Project Structure**

- `Dockerfile`: Contains the Docker instructions to build the RabbitMQ image.
- `docker-compose.yml`: Used to define and run the RabbitMQ service.
- `rabbitmq.conf`: RabbitMQ server configuration file.
- `data/`: Contains the RabbitMQ server data, including the Erlang cookie, Mnesia database, and schema.
- `logs/`: Contains the server logs and crash dumps.
- `plugins/`: Contains the RabbitMQ server plugins and related configuration files.

## **Setup**

To set up the RabbitMQ server, you need to have Docker and Docker Compose installed on your machine. Once you have those installed, you can start the server by running:

```
docker-compose up

```

This will start the RabbitMQ server in a Docker container. You can then access the RabbitMQ management interface at [http://localhost:15672](http://localhost:15672/).

License This project is licensed under the MIT License - see the LICENSE.md file for details.

## **Docker File**

```
FROM rabbitmq:3.7-management

COPY ./plugins/rabbitmq.conf /etc/rabbitmq
COPY ./plugins/definitions.json /etc/rabbitmq
COPY ./plugins/advanced.config /etc/rabbitmq

RUN cat /etc/rabbitmq/rabbitmq.conf
EXPOSE 15671 15672 61613 61614 1883 8883 15675 5672 15670 15674

```

## **Docker Compose**

```yaml
version: "3.2"
services:
  rabbitmq:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: "rabbitmq"
    ports:
      - 5672:5672
      - 15672:15672
      - 15674:15674
      - 15670:15670
      - 15673:15673
      - 61613:61613
    volumes:
      - ./data/:/var/lib/rabbitmq
      - ./logs/:/var/log/rabbitmq
      - ./plugins:/etc/rabbitmq
    restart: unless-stopped

```

## **Enabled Plugins**

For more information on RabbitMQ Erlang Plugins file, please refer to **[Plugins](https://www.rabbitmq.com/plugins.html#enabled-plugins-file)**

## Stomp & Stomp-Web Configuration

## 

## Advanced Config

> ./plugins/advanced.config ⇒ /etc/rabbitmq/advanced.config
> 

This configuration shapes broker behavior, user access, and network settings to enhance performance and resource management. It establishes the STOMP plugin with designated user credentials and TCP parameters, activates the management plugin on a specific port, configures the RabbitMQ server to listen for AMQP connections on the default port, and sets the Web STOMP plugin to necessitate explicit connections.

1. **rabbitmq_stomp**: Sets up the STOMP plugin, including default user credentials and TCP options (buffer sizes, timeouts, keepalive, etc.).
2. **rabbitmq_management**: Enables the management plugin, setting the port to 15672.
3. **rabbit**: Defines general server settings, such as AMQP listener ports and loopback user restrictions.
4. **rabbitmq_web_stomp**: Configures the Web STOMP plugin, requiring explicit client connections.

```erlang
[{rabbitmq_stomp,[{default_user,[{passcode,"guest"},{login,"guest"}]},
                  {tcp_listen_options,[{sndbuf,131072},
                                       {send_timeout,120},
                                       {recbuf,131072},
                                       {keepalive,true},
                                       {exit_on_close,true},
                                       {nodelay,true},
                                       {backlog,4096}]},
                  {default_vhost,<<"/">>}]},
 {rabbitmq_management,[{tcp_config,[{port,15672}]}]},
 {rabbit,[{tcp_listeners,[5672]},{loopback_users,[]}]},
 {rabbitmq_web_stomp,[{implicit_connect,false}]}].
```