FROM rabbitmq:3.7-management

COPY ./plugins/rabbitmq.conf /etc/rabbitmq
COPY ./plugins/definitions.json /etc/rabbitmq
COPY ./plugins/advanced.config /etc/rabbitmq

RUN cat /etc/rabbitmq/rabbitmq.conf
EXPOSE 15671 15672 61613 61614 1883 8883 15675 5672 15670 15674
